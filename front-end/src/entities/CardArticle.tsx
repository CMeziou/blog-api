export interface CardArticle {
    id?:number;
    image?:string;
    title?:string;
    author?:string;
    date?: string;
    viewsNb?: number;
    paragraph?: string;
}
