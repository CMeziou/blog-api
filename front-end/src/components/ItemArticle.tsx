import { CardArticle } from "@/entities/CardArticle";
import Link from "next/link";

interface Props {
    cardArticle: CardArticle;
}
export default function ItemArticle({ cardArticle }: Props) {
    return (
        <>
            <div className="col-sm-6 col-md-6 col-lg-4">
                <div className="card m-2 p-1 text-center">
                    <img src={cardArticle.image} className="card-img-top" alt="img"></img>
                    <div className="card-body">
                        <h5 className="card-title">{cardArticle.title}</h5>
                        <h6>Publié par {cardArticle.author}</h6>
                        {cardArticle?.date &&
                            <p>Le: {new Date(cardArticle.date).toLocaleDateString()}</p>
                        }
                        {cardArticle?.paragraph &&
                            <p>{cardArticle.paragraph.split(" ").slice(0, 4).join(" ")}...</p>
                        }
                        <Link href={"/article/" + cardArticle.id} className="btn btn-secondary mt-5">Lire la suite</Link>
                    </div>
                </div>
            </div>
        </>
    )
}