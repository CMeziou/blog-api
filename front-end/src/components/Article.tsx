import { CardArticle } from "@/entities/CardArticle";

export default function Articles(Article: CardArticle) {
    return (
        <>
            <div className="container-fluid bg-info">
                <div className="row">
                    <div className="col">

                <h1>{Article.title}</h1>
                <img src={Article.image} className="card-img-top" alt="img"></img>
                <div className="card-body">
                    <p className="p-5 fst-italic fw-semibold">{Article.paragraph}</p>
                </div>
                    </div>
                </div>
            </div>
        </>
    );
}

