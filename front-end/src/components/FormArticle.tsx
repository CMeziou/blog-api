import { CardArticle } from "@/entities/CardArticle";
import { postArticle } from "articles-service";
import { useRouter } from "next/router";
import { FormEvent, useState } from "react"

interface Props {
    onSubmit:(article:CardArticle) => void;
    edited?:CardArticle;
}


export default function FormArticle({onSubmit, edited}:Props) {

    const router = useRouter();
    const[errors, setErrors]=useState('');
    const [article, setArticle] = useState<CardArticle>(edited?edited:{
        image: '',
        title: '',
        author:'',
        date: '',
        paragraph: ''
    });

    function handleChange(event: any) {
        setArticle({
            ...article,
            [event.target.name]: event.target.value
        });
    }

    async function handleSubmit(event:FormEvent) {
        event.preventDefault();
        // const added =  await postArticle(article);
        // router.push('/article/'+added.id);
        try{
            onSubmit(article);
        }
        catch(error:any){
            if(error.response.status == 400){
                setErrors(error.response.data.detail);
            }
        }

    }


    return (
        <form onSubmit={handleSubmit}>
            <label htmlFor="image">Image :</label>
            <input type="text" name="image" value={article.image} onChange={handleChange} required/>
            <label htmlFor="title">Title :</label>
            <input type="text" name="title" value={article.title} onChange={handleChange} required/>
            <label htmlFor="date">Date :</label>
            <input type="date" name="date" value={article.date} onChange={handleChange} required/>
            <label htmlFor="paragraph">Description :</label>
            <input type="textarea" name="paragraph" value={article.paragraph} onChange={handleChange} required/>

            <button>Submit</button>
        </form>
    )
}
