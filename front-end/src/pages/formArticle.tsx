import FormArticle from "@/components/FormArticle";
import { CardArticle } from "@/entities/CardArticle";
import Link from "next/link";
import article from "./article/[id]";

export default function form() {
    return (
        <>
            <Link href="/">
                <img className="mx-auto d-block p-5" src="https://media.cdnws.com/_i/136023/1221/2641/23/logo-la-boutique-one-piece-v2-v5-le-petit.jpeg.webp" alt="logo" />
            </Link>
            <div className="container">
                <h2 className="h2 text-center m-4">Publié un article!</h2>
                <h4>Ajouter</h4>
                <div className="my-5">
                    <FormArticle onSubmit={article} />
                </div>
            </div>
        </>


    )
}