import { useEffect, useState } from "react";
import { deleteArticle, fetchOneArticle, updateArticle } from "articles-service";
import { useRouter } from "next/router";
import Article from "@/components/Article";
import Link from "next/link";
import { CardArticle } from "@/entities/CardArticle";
import FormArticle from "@/components/FormArticle";



export default function article() {
    const router = useRouter();
    const { id } = router.query;

    const [article, setArticle] = useState<CardArticle>();
    const [showEdit, setShowEdit] = useState(false);

    useEffect(() => {
        if (!id) {
            return;
        }
        fetchOneArticle(Number(id))
            .then(data => setArticle(data))
            .catch(error => {
                //Si on a une erreur 404 on redirige vers la page 404 par défaut
                //Pas idéal, mais on fera un truc mieux quand on fera du SSR avec next
                console.log(error);
                if (error.response.status == 404) {
                    router.push('/404');
                }
            });


    }, [id]);

    async function remove() {
        await deleteArticle(id);
        router.push('/');
    }

    async function update(article:CardArticle) {
        const updated = await updateArticle(article);
        setArticle(updated);
    }

    function toggle() {
        setShowEdit(!showEdit);
    }



    if (!article) {
        return <p>Loading...</p>
    }


    return (
        <>
            <Link href="/" className="mx-auto d-flex justify-content-center w-25">
                <img className="mx-auto d-block p-5" src="https://media.cdnws.com/_i/136023/1221/2641/23/logo-la-boutique-one-piece-v2-v5-le-petit.jpeg.webp" alt="logo"/>
            </Link>
            <div className="row w-100 text-center m-auto p-3 m-3">
                {/* <h1>10 Choses à savoir sur One Piece : Red</h1> */}
                {/* {article.map((item) => */}

                <Article key={article.id} title={article.title} image={article.image} paragraph={article.paragraph} />
        
                {/* )} */}
            </div>
            <button onClick={remove}>Delete</button>
            <button onClick={toggle}>Edit</button>

            {showEdit &&
                <>
                    <h2>Edit dog</h2>
                    <FormArticle edited={article} onSubmit={update} />
                </>
            }

        </>
    )
}

















// { title: "Que relate le scénario de One Piece : Red ?", image: "https://www.laboutique-onepiece.com/_i/136023/p%7B1000%7D-89617/3457/18/10-choses-savoir-one-piece-red-film-blog-8.jpeg", paragraph: "Le film parle essentiellement d’Uta dont la voix est décrite comme celle « d’un autre monde ». Elle tient son tout premier concert live et se révèle au public où le monde se rassemble pour profiter de sa belle voix.La fièvre de l’événement a ainsi rassemblé un grand monde au même endroit : les Chapeaux de Paille, les pirates, les Marines et ses fans du monde. Cependant, l’événement commence par la révélation choquante qu’Uta est la fille de Shanks. Cela conduit à une série d’événements intéressants qui impliquent un duel dynamique entre l’Équipage du Roux et les Marines.C’est une idée générale de ce dont parle le film, mais pour notre plus grand bonheur, il réserve des surprises qui vous laisseront bouche bée. Et dans cet article, nous discuterons de certaines des révélations intéressantes de One Piece : Red qui sont totalement époustouflantes.Alors, sans plus tarder, commençons notre top 10 comme il se doit :" },
// { title: "10 Choses à savoir sur One Piece : Red", image: "https://www.laboutique-onepiece.com/_i/136023/p%7B1000%7D-89624/468/25/10-choses-savoir-one-piece-red-film-blog-14.jpeg", paragraph: "Depuis la première sortie du film One Piece : Red, les avis des fans de la saga culte se succèdent intensément. Que ce soit par son scénario accrocheur, ou ses personnages, le film fait sensation en ce moment. Malgré sa diffusion uniquement au Japon et en France, des records d’audiences sont atteints et le box-office explose à mesure que le temps avance. Vous allez l’adorer !" },
// { title: "10 Choses à savoir sur One Piece : Red", image: "https://www.laboutique-onepiece.com/_i/136023/p%7B1000%7D-89624/468/25/10-choses-savoir-one-piece-red-film-blog-14.jpeg", paragraph: "Depuis la première sortie du film One Piece : Red, les avis des fans de la saga culte se succèdent intensément. Que ce soit par son scénario accrocheur, ou ses personnages, le film fait sensation en ce moment. Malgré sa diffusion uniquement au Japon et en France, des records d’audiences sont atteints et le box-office explose à mesure que le temps avance. Vous allez l’adorer !" },
// { title: "10 Choses à savoir sur One Piece : Red", image: "https://www.laboutique-onepiece.com/_i/136023/p%7B1000%7D-89624/468/25/10-choses-savoir-one-piece-red-film-blog-14.jpeg", paragraph: "Depuis la première sortie du film One Piece : Red, les avis des fans de la saga culte se succèdent intensément. Que ce soit par son scénario accrocheur, ou ses personnages, le film fait sensation en ce moment. Malgré sa diffusion uniquement au Japon et en France, des records d’audiences sont atteints et le box-office explose à mesure que le temps avance. Vous allez l’adorer !" },
// { title: "10 Choses à savoir sur One Piece : Red", image: "https://www.laboutique-onepiece.com/_i/136023/p%7B1000%7D-89624/468/25/10-choses-savoir-one-piece-red-film-blog-14.jpeg", paragraph: "Depuis la première sortie du film One Piece : Red, les avis des fans de la saga culte se succèdent intensément. Que ce soit par son scénario accrocheur, ou ses personnages, le film fait sensation en ce moment. Malgré sa diffusion uniquement au Japon et en France, des records d’audiences sont atteints et le box-office explose à mesure que le temps avance. Vous allez l’adorer !" },
// { title: "10 Choses à savoir sur One Piece : Red", image: "https://www.laboutique-onepiece.com/_i/136023/p%7B1000%7D-89624/468/25/10-choses-savoir-one-piece-red-film-blog-14.jpeg", paragraph: "Depuis la première sortie du film One Piece : Red, les avis des fans de la saga culte se succèdent intensément. Que ce soit par son scénario accrocheur, ou ses personnages, le film fait sensation en ce moment. Malgré sa diffusion uniquement au Japon et en France, des records d’audiences sont atteints et le box-office explose à mesure que le temps avance. Vous allez l’adorer !" },
// { title: "10 Choses à savoir sur One Piece : Red", image: "https://www.laboutique-onepiece.com/_i/136023/p%7B1000%7D-89624/468/25/10-choses-savoir-one-piece-red-film-blog-14.jpeg", paragraph: "Depuis la première sortie du film One Piece : Red, les avis des fans de la saga culte se succèdent intensément. Que ce soit par son scénario accrocheur, ou ses personnages, le film fait sensation en ce moment. Malgré sa diffusion uniquement au Japon et en France, des records d’audiences sont atteints et le box-office explose à mesure que le temps avance. Vous allez l’adorer !" },
// { title: "10 Choses à savoir sur One Piece : Red", image: "https://www.laboutique-onepiece.com/_i/136023/p%7B1000%7D-89624/468/25/10-choses-savoir-one-piece-red-film-blog-14.jpeg", paragraph: "Depuis la première sortie du film One Piece : Red, les avis des fans de la saga culte se succèdent intensément. Que ce soit par son scénario accrocheur, ou ses personnages, le film fait sensation en ce moment. Malgré sa diffusion uniquement au Japon et en France, des records d’audiences sont atteints et le box-office explose à mesure que le temps avance. Vous allez l’adorer !" },
// { title: "10 Choses à savoir sur One Piece : Red", image: "https://www.laboutique-onepiece.com/_i/136023/p%7B1000%7D-89624/468/25/10-choses-savoir-one-piece-red-film-blog-14.jpeg", paragraph: "Depuis la première sortie du film One Piece : Red, les avis des fans de la saga culte se succèdent intensément. Que ce soit par son scénario accrocheur, ou ses personnages, le film fait sensation en ce moment. Malgré sa diffusion uniquement au Japon et en France, des records d’audiences sont atteints et le box-office explose à mesure que le temps avance. Vous allez l’adorer !" },
// { title: "10 Choses à savoir sur One Piece : Red", image: "https://www.laboutique-onepiece.com/_i/136023/p%7B1000%7D-89624/468/25/10-choses-savoir-one-piece-red-film-blog-14.jpeg", paragraph: "Depuis la première sortie du film One Piece : Red, les avis des fans de la saga culte se succèdent intensément. Que ce soit par son scénario accrocheur, ou ses personnages, le film fait sensation en ce moment. Malgré sa diffusion uniquement au Japon et en France, des records d’audiences sont atteints et le box-office explose à mesure que le temps avance. Vous allez l’adorer !" },