import ItemArticle from "@/components/ItemArticle";
import { CardArticle } from "@/entities/CardArticle";
import { fetchAllArticle } from "articles-service";
import Link from "next/link";
import { useEffect, useState } from "react";

export default function Index() {
  const [article, setArticle] = useState<CardArticle[]>([]);

  useEffect(() => {
    fetchAllArticle().then(data => {
      setArticle(data);
    })
  }, [])

  return (
    <>

      <Link href="/" className="mx-auto d-flex justify-content-center w-25">
        <img className=" img-fluid" src="https://media.cdnws.com/_i/136023/1221/2641/23/logo-la-boutique-one-piece-v2-v5-le-petit.jpeg.webp" alt="logo" />
      </Link>
      <h1 className="text-center">Blog ONE PIECE</h1>

      <div className="container">
        <div className="row d-flex justify-content-center ">

          {article.map(item =>

            <ItemArticle key={item.id} cardArticle={item} />

          )}
          <Link href={"/formArticle"}>
          <button >Ajouter</button>
          </Link>
        
        </div>
      </div>

    </>
  )

}