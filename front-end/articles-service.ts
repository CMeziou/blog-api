import { CardArticle } from "@/entities/CardArticle";
import axios from "axios";


export async function fetchAllArticle() {
    const response = await axios.get<CardArticle[]>('http://localhost:8000/api/article');
    return response.data;
}
export async function fetchOneArticle(id: number) {
    const response = await axios.get<CardArticle>('http://localhost:8000/api/article/' + id);
    return response.data;
}

export async function postArticle(article: CardArticle) {
    const response = await axios.post<CardArticle>('http://localhost:8000/api/article', article);
    return response.data;
}

export async function updateArticle(article:CardArticle) {
    const response = await axios.put<CardArticle>('http://localhost:8000/api/article/'+article.id, article);
    return response.data;
}

export async function deleteArticle(id:any) {
    await axios.delete('http://localhost:8000/api/article/'+id);
}

