### Projet Blog One Piece - Symfony Backend & Next.js Frontend

## Description
Le projet Blog One Piece combine le framework Symfony pour le backend et Next.js pour le frontend, créant ainsi un blog dédié à l'univers de One Piece. Le backend utilise Symfony avec trois entités principales : Article, Category et Comment, chacune avec son propre repository et controller. Du côté frontend, Next.js est utilisé pour consommer l'API Symfony en utilisant Axios, avec des composants React, useState et serverSideProps pour une expérience utilisateur fluide.

## Backend (Symfony)

### Entités
1. **Article:** Représente les articles du blog avec des champs tels que le titre, le contenu, la date de publication, etc.
2. **Category:** Catégorise les articles en fonction de leur thème ou sujet.
3. **Comment:** Les commentaires associés à chaque article avec des informations sur l'auteur, le contenu et la date.

### Controllers
1. **ArticleController:** Gère les opérations liées aux articles, y compris l'affichage de tous les articles, la création, la mise à jour et la suppression.
2. **CategoryController:** Gère les opérations liées aux catégories, notamment l'affichage de toutes les catégories et la gestion des relations avec les articles.
3. **CommentController:** Gère les opérations liées aux commentaires, telles que l'affichage, la création et la suppression.

### Installation
1. Clonez le dépôt `git clone https://github.com/votre_utilisateur/blog-one-piece-backend.git`
2. Installez les dépendances avec Composer `composer install`
3. Configurez la base de données dans le fichier `.env` et exécutez les migrations `php bin/console doctrine:migrations:migrate`

## Frontend (Next.js)

### Composants
1. **ArticleList:** Affiche la liste des articles avec leurs titres et résumés.
2. **CategoryFilter:** Permet aux utilisateurs de filtrer les articles par catégorie.
3. **CommentSection:** Affiche les commentaires associés à un article spécifique.

### Installation
1. Clonez le dépôt `git clone https://github.com/votre_utilisateur/blog-one-piece-frontend.git`
2. Installez les dépendances avec npm `npm install`
3. Lancez l'application avec `npm run dev`

## Auteurs
Chems MEZIOU