<?php

namespace App\Controller;

use App\Entities\Article;
use App\Repository\ArticleRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Exception\NotEncodableValueException;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Exception\ValidationFailedException;

#[Route('/api/article')]

class ArticleController extends AbstractController{


    private ArticleRepository $art;
    
    
    /**
     * @param ArticleRepository $art
     */
    public function __construct(ArticleRepository $art) {
        $this->art = $art;
    }
    
    #[Route(methods:'GET')]
   
    public function all()
    {
        $art = $this->art->findAll();
        return $this->json($art);
    }
    #[Route('/{id}', methods: 'GET')]
    public function byId(int $id){
        $article = $this->art->findById($id);
        if(!$article){
            throw new NotFoundHttpException();
        }
        return $this->json($article);
    }

    #[Route(methods: 'POST')]
    public function add(Request $request, SerializerInterface $serializer) {
        try{
            $article = $serializer->deserialize($request->getContent(), Article::class, 'json');
            $this->art->persist($article);
    
            return $this->json($article, Response::HTTP_CREATED);
        }catch (ValidationFailedException $e) {
            return $this->json($e->getViolations(), Response::HTTP_BAD_REQUEST);
        } catch (NotEncodableValueException $e) {
            return $this->json('Invalid json', Response::HTTP_BAD_REQUEST);
        }


    }

    #[Route('/{id}', methods: 'PUT')]
    public function put(int $id, Request $request, SerializerInterface $serializer) {   
        $art = $this->art->findById($id);
        if(!$art){
            throw new NotFoundHttpException();
        }

        try{
            $toUpdate = $serializer->deserialize($request->getContent(), Article::class, 'json');
            $toUpdate->setId($id);
            $this->art->update($toUpdate);
    
            return $this->json($toUpdate);
        }catch (ValidationFailedException $e) {
            return $this->json($e->getViolations(), Response::HTTP_BAD_REQUEST);
        } catch (NotEncodableValueException $e) {
            return $this->json('Invalid json', Response::HTTP_BAD_REQUEST);
        }


        
    }

    #[Route('/{id}', methods: 'DELETE')]
    public function remove(int $id) {   
        $art = $this->art->findById($id);
        if(!$art){
            throw new NotFoundHttpException();
        }

        $this->art->delete($art);

        return $this->json(null, Response::HTTP_NO_CONTENT);
        
    }


}
