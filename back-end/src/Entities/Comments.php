<?php

namespace App\Entities;

use DateTime;


class Comments{
    private ?int $id;
    private ?string $pseudo;
    private ?string $comment;
    private ?DateTime $date;
    private ?int $idArticle;

    /**
     * @param int|null $id
     * @param string|null $pseudo
     * @param string|null $comment
     * @param DateTime|null $date
     * @param int|null $idArticle
     */
    public function __construct(?int $id, ?string $pseudo, ?string $comment, ?DateTime $date, ?int $idArticle) {
    	$this->id = $id;
    	$this->pseudo = $pseudo;
    	$this->comment = $comment;
    	$this->date = $date;
    	$this->idArticle = $idArticle;
    }

	/**
	 * @return int|null
	 */
	public function getId(): ?int {
		return $this->id;
	}
	
	/**
	 * @param int|null $id 
	 * @return self
	 */
	public function setId(?int $id): self {
		$this->id = $id;
		return $this;
	}
	
	/**
	 * @return string|null
	 */
	public function getPseudo(): ?string {
		return $this->pseudo;
	}
	
	/**
	 * @param string|null $pseudo 
	 * @return self
	 */
	public function setPseudo(?string $pseudo): self {
		$this->pseudo = $pseudo;
		return $this;
	}
	
	/**
	 * @return string|null
	 */
	public function getComment(): ?string {
		return $this->comment;
	}
	
	/**
	 * @param string|null $comment 
	 * @return self
	 */
	public function setComment(?string $comment): self {
		$this->comment = $comment;
		return $this;
	}
	
	/**
	 * @return DateTime|null
	 */
	public function getDate(): ?DateTime {
		return $this->date;
	}
	
	/**
	 * @param DateTime|null $date 
	 * @return self
	 */
	public function setDate(?DateTime $date): self {
		$this->date = $date;
		return $this;
	}
	
	/**
	 * @return int|null
	 */
	public function getIdArticle(): ?int {
		return $this->idArticle;
	}
	
	/**
	 * @param int|null $idArticle 
	 * @return self
	 */
	public function setIdArticle(?int $idArticle): self {
		$this->idArticle = $idArticle;
		return $this;
	}
}