<?php

namespace App\Entities;

class Category{
    private ?int $id;
    private ?string $title;

    /**
     * @param int|null $id
     * @param string|null $title
     */
    public function __construct(?int $id, ?string $title) {
    	$this->id = $id;
    	$this->title = $title;
    }

	/**
	 * @return int|null
	 */
	public function getId(): ?int {
		return $this->id;
	}
	
	/**
	 * @param int|null $id 
	 * @return self
	 */
	public function setId(?int $id): self {
		$this->id = $id;
		return $this;
	}
	
	/**
	 * @return string|null
	 */
	public function getTitle(): ?string {
		return $this->title;
	}
	
	/**
	 * @param string|null $title 
	 * @return self
	 */
	public function setTitle(?string $title): self {
		$this->title = $title;
		return $this;
	}
}