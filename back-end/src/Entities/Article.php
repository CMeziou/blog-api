<?php

namespace App\Entities;
use Symfony\Component\Validator\Constraints as Assert;

use DateTime;
class Article{
    private ?int $id;
	#[Assert\NotBlank]
    private ?string $image;
	#[Assert\NotBlank]
    private ?string $title;
	#[Assert\NotBlank]
    private ?string $author;
	#[Assert\Date]
    private ?DateTime $date;
	#[Assert\NotBlank(allowNull: true)]
    private ?int $viewsNb;
	#[Assert\NotBlank]
    private ?string $paragraph;

    /**
     * @param int|null $id
     * @param string|null $image
     * @param string|null $title
     * @param string|null $author
     * @param DateTime|null $date
     * @param int|null $viewsNb
     * @param string|null $paragraph
     */
    public function __construct(?string $image, ?string $title, ?string $author, ?DateTime $date , ?int $viewsNb, ?string $paragraph, ?int $id) {
    	$this->id = $id;
    	$this->image = $image;
    	$this->title = $title;
    	$this->author = $author;
    	$this->date = $date;
    	$this->viewsNb = $viewsNb;
    	$this->paragraph = $paragraph;
    }

	/**
	 * @return int|null
	 */
	public function getId(): ?int {
		return $this->id;
	}
	
	/**
	 * @param int|null $id 
	 * @return self
	 */
	public function setId(?int $id): self {
		$this->id = $id;
		return $this;
	}
	
	/**
	 * @return string|null
	 */
	public function getImage(): ?string {
		return $this->image;
	}
	
	/**
	 * @param string|null $image 
	 * @return self
	 */
	public function setImage(?string $image): self {
		$this->image = $image;
		return $this;
	}
	
	/**
	 * @return string|null
	 */
	public function getTitle(): ?string {
		return $this->title;
	}
	
	/**
	 * @param string|null $title 
	 * @return self
	 */
	public function setTitle(?string $title): self {
		$this->title = $title;
		return $this;
	}
	
	/**
	 * @return string|null
	 */
	public function getAuthor(): ?string {
		return $this->author;
	}
	
	/**
	 * @param string|null $author 
	 * @return self
	 */
	public function setAuthor(?string $author): self {
		$this->author = $author;
		return $this;
	}
	
	/**
	 * @return DateTime|null
	 */
	public function getDate(): ?DateTime {
		return $this->date;
	}
	
	/**
	 * @param DateTime|null $date 
	 * @return self
	 */
	public function setDate(?DateTime $date): self {
		$this->date = $date;
		return $this;
	}
	
	/**
	 * @return int|null
	 */
	public function getViewsNb(): ?int {
		return $this->viewsNb;
	}
	
	/**
	 * @param int|null $viewsNb 
	 * @return self
	 */
	public function setViewsNb(?int $viewsNb): self {
		$this->viewsNb = $viewsNb;
		return $this;
	}
	
	/**
	 * @return string|null
	 */
	public function getParagraph(): ?string {
		return $this->paragraph;
	}
	
	/**
	 * @param string|null $paragraph 
	 * @return self
	 */
	public function setParagraph(?string $paragraph): self {
		$this->paragraph = $paragraph;
		return $this;
	}
}