<?php

namespace App\Serializer;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Validator\ConstraintViolationList;
use Symfony\Component\Validator\Exception\ValidationFailedException;
use Symfony\Component\Validator\Validator\ValidatorInterface;


class ValidatorDenormalizer implements DenormalizerInterface
{

    public function __construct( private ObjectNormalizer $normalizer, private ValidatorInterface $validator)
    {}
	/**
	 * Denormalizes data back into an object of the given class.
	 *
	 * @param mixed $data Data to restore
	 * @param string $type The expected class to instantiate
	 * @param null|string $format Format the given data was extracted from
	 * @param array $context Options available to the denormalizer
	 * @return mixed
	 */
	public function denormalize(mixed $data, string $type, string $format = null, array $context = array()) {
        $errors = new ConstraintViolationList();
        $reflection = new \ReflectionClass($type);
        $props =$reflection->getProperties();
        foreach ($props as $prop) {
            $value = null;
            if(isset($data[$prop->name])) {
                $value = $data[$prop->name];
            }
            $errors->addAll($this->validator->validatePropertyValue($type,$prop->name,$value));

        }
        if($errors->count() > 0) {
            throw new ValidationFailedException('Validation error', $errors);
        }
        return $this->normalizer->denormalize($data,$type,$format,$context);
	}
	
	/**
	 * Checks whether the given class is supported for denormalization by this normalizer.
	 *
	 * @param mixed $data Data to denormalize from
	 * @param string $type The class to which the data should be denormalized
	 * @param null|string $format The format being deserialized from
	 * @return bool
	 */
	public function supportsDenormalization(mixed $data, string $type, string $format = null) {
        return true;
	}
}