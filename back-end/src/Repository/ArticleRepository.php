<?php

namespace App\Repository;

use App\Entities\Article;
use PDO;
use DateTime;

class ArticleRepository
{
    private PDO $connection;

    public function __construct()
    {
        $this->connection = Database::connect();
    }

    public function findAll(): array
    {
        $article = [];
        $statement = $this->connection->prepare('SELECT * FROM article');

        $statement->execute();

        $results = $statement->fetchAll();
        foreach ($results as $item) {
            $article[] = $this->sqlToArticle($item);

            // new Article($item['id'],$item['https://www.laboutique-onepiece.com/_i/136023/p%7B1000%7D-89624/468/25/10-choses-savoir-one-piece-red-film-blog-14.jpeg'], $item['Film Red'], $item['Echiro Oda'], $item['2022-08-20'], $item[230000], $item['Depuis la première sortie du film One Piece : Red, les avis des fans de la saga culte se succèdent intensément. Que ce soit par son scénario accrocheur, ou ses personnages, le film fait sensation en ce moment. Malgré sa diffusion uniquement au Japon et en France, des records d’audiences sont atteints et le box-office explose à mesure que le temps avance. Vous allez l’adorer !

            // Voici pour vous un top 10 des choses à savoir de One Piece : Red']);
        }


        return $article;
    }

    public function findById(int $id){
        $statement = $this->connection->prepare('SELECT * FROM article WHERE id=:id');
        $statement->bindValue('id', $id);

        $statement->execute();

        $result = $statement->fetch();
        if($result){
            return $this->sqlToArticle($result);
        }
        return null;
    }

    public function persist(Article $article){
        $statement = $this->connection->prepare('INSERT INTO article(image,title,author,date,viewsNb, paragraph) VALUES (:image, :title, :author, :date, :viewsNb, :paragraph)');
        $statement->bindValue('image', $article->getImage());
        $statement->bindValue('title', $article->getTitle());
        $statement->bindValue('author', $article->getAuthor());
        $statement->bindValue('date', $article->getDate()->format('Y-m-d'));
        $statement->bindValue('viewsNb', $article->getViewsNb());
        $statement->bindValue('paragraph', $article->getParagraph());

        $statement->execute();

        $article->setId($this->connection->lastInsertId());
    }
    public function update(Article $article) {
        $statement = $this->connection->prepare('UPDATE article SET image=:image,title=:title,author=:author,date=:date,viewsNb=:viewsNb, paragraph=:paragraph WHERE id=:id');
        $statement->bindValue('image', $article->getImage());
        $statement->bindValue('title', $article->getTitle());
        $statement->bindValue('author', $article->getAuthor());
        $statement->bindValue('date', $article->getDate()->format('Y-m-d'));
        $statement->bindValue('viewsNb', $article->getViewsNb());
        $statement->bindValue('paragraph', $article->getParagraph());
        $statement->bindValue('id', $article->getId(), PDO::PARAM_INT);


        $statement->execute();

    }
    public function delete(Article $article) {
        $statement = $this->connection->prepare('DELETE FROM article WHERE id=:id');
        $statement->bindValue('id', $article->getId(), PDO::PARAM_INT);


        $statement->execute();

    }


    public function sqlToArticle(array $line): Article
    {

        $date = null;
        if (isset($line['date'])) {
            $date = new DateTime($line['date']);
        }

        return new Article($line['image'], $line['title'], $line['author'], $line['date']= null, $line['viewsNb'], $line['paragraph'], $line['id']);
    }
}