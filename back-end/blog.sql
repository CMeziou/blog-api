-- Active: 1673948012430@@127.0.0.1@3306@blog

DROP TABLE IF EXISTS article_category;
DROP TABLE IF EXISTS comments;
DROP TABLE IF EXISTS category;

DROP TABLE IF EXISTS article;

CREATE TABLE
    article(
        id INT PRIMARY KEY AUTO_INCREMENT,
        image VARCHAR (500) NOT NULL,
        title VARCHAR(250) NOT NULL,
        author VARCHAR(250) NOT NULL,
        date DATE,
        viewsNb INT,
        paragraph TEXT
    );

CREATE TABLE
    comments(
        id INT PRIMARY KEY AUTO_INCREMENT,
        pseudo VARCHAR(250) NOT NULL,
        comment TEXT NOT NULL,
        date DATE,
        id_article INT,
        Foreign Key (id_article) REFERENCES article(id) ON DELETE SET NULL
    );

CREATE TABLE
    category(
        id INT PRIMARY KEY AUTO_INCREMENT,
        title VARCHAR(250)
    );

CREATE TABLE 
    article_category(
        article_id INT,
        category_id INT,
        PRIMARY KEY (article_id, category_id),
        Foreign Key (article_id) REFERENCES article(id),
        Foreign Key (category_id) REFERENCES category(id)
    );


INSERT INTO article(image, title, author, date, viewsNb, paragraph) VALUES('https://www.laboutique-onepiece.com/_i/136023/p%7B1000%7D-89624/468/25/10-choses-savoir-one-piece-red-film-blog-14.jpeg', 'One Piece RED', 'Echiro Oda', "1996-06-14", 1000, 'Depuis la première sortie du film One Piece : Red, les avis des fans de la saga culte se succèdent intensément. Que ce soit par son scénario accrocheur, ou ses personnages, le film fait sensation en ce moment. Malgré sa diffusion uniquement au Japon et en France, des records d’audiences sont atteints et le box-office explose à mesure que le temps avance. Vous allez l’adorer! Voici pour vous un top 10 des choses à savoir de One Piece : Red');

INSERT INTO comments(pseudo, comment, date, id_article) VALUES ('Chems', 'Uta est la fille adoptive de Shanks. Elle a été retrouvée par Shanks dans une boîte au trésor quand elle était bébé. Cependant, il a décidé de l’abandonner pour qu’elle puisse vivre libre et ne soit pas ciblée pour ses pouvoirs.', "2023-02-08", 1);

INSERT INTO category(title) VALUES('Manga');